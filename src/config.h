/*
 * The MySensors Arduino library handles the wireless radio link and protocol
 * between your home built sensors/actuators and HA controller of choice.
 * The sensors forms a self healing radio network with optional repeaters. Each
 * repeater and gateway builds a routing tables in EEPROM which keeps track of the
 * network topology allowing messages to be routed to nodes.
 *
 * Created by Henrik Ekblad <henrik.ekblad@mysensors.org>
 * Copyright (C) 2013-2019 Sensnology AB
 * Full contributor list: https://github.com/mysensors/MySensors/graphs/contributors
 * or https://github.com/mysensors/Arduino/graphs/contributors
 *
 * Documentation: http://www.mysensors.org
 * Support Forum: http://forum.mysensors.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 *******************************
 *
 * REVISION HISTORY
 * Version 1.1 - 2016-07-20: Converted to MySensors v2.0 and added various improvements - Torben Woltjen (mozzbozz) 
 * Version 1.0 - Henrik Ekblad
 *
 * DESCRIPTION
 * Example sketch showing how to control physical relays.
 * This example will remember relay state after power failure.
 * http://www.mysensors.org/build/relay
 */

/* Enable and select radio type attached */
#define MY_RADIO_RF24
// #define MY_RADIO_RFM69
// #define MY_RS485
// #define MY_RADIO_RFM95
// #define MY_RADIO_NRF5_ESB

/* NRF24L01 advanced settings */
// #define MY_RF24_IRQ_PIN 2
// #define MY_RX_MESSAGE_BUFFER_FEATURE
// #define MY_RX_MESSAGE_BUFFER_SIZE 35

/* Load core modules only */
// #define MY_CORE_ONLY

/* RF_Nano board */
#define RF_NANO

/* Enable repeater functionality for this node */
#define MY_REPEATER_FEATURE

/* Enable debug prints to serial monitor */
// #define MY_DEBUG
// #define MY_DEBUG_VERBOSE_RF24

/* Security */
// #define MY_SIGNING_SIMPLE_PASSWD "changeme"

/* Enable AES encryption */
// #define MY_RF24_ENABLE_ENCRYPTION

/* Enable message signing */
// #define MY_SIGNING_SOFT
// #define MY_SIGNING_SOFT_RANDOMSEED_PIN A0   // Not longer required since the CPU integrated random numger generator is used
// #define MY_VERIFICATION_TIMEOUT_MS 5000
// #define MY_SIGNING_REQUEST_SIGNATURES
// #define MY_SIGNING_GW_REQUEST_SIGNATURES_FROM_ALL

/* RF_Nano board settings */
#ifdef RF_NANO
#define MY_RF24_CE_PIN 10 
#define MY_RF24_CS_PIN 9
#endif

/* Print functions used in debug mode */
#ifdef MY_DEBUG
#define DEBUG_PRINT(x) Serial.print(x)
#define DEBUG_PRINTLN(x) Serial.println(x)
#else
#define DEBUG_PRINT(x)
#define DEBUG_PRINTLN(x)
#endif

/**
* @def MY_DEBUG_VERBOSE_GATEWAY
* @brief Define this for verbose debug prints related to the gateway transport.
*/
// #define MY_DEBUG_VERBOSE_GATEWAY

/**
* @def MY_REGISTRATION_DEFAULT
* @brief Node registration default - this applies if no registration response is received from
 *       controller.
*/
// #define MY_REGISTRATION_DEFAULT (true)

/* MySensors: Choose your desired radio power level. High power can cause issues on cheap Chinese NRF24 radio's. */
// #define MY_RF24_PA_LEVEL RF24_PA_MIN
// #define MY_RF24_PA_LEVEL RF24_PA_LOW
// #define MY_RF24_PA_LEVEL RF24_PA_HIGH
// #define MY_RF24_PA_LEVEL RF24_PA_MAX

/* Mysensors advanced settings */
#define MY_TRANSPORT_UPLINK_CHECK_DISABLED
#define MY_TRANSPORT_WAIT_READY_MS 10000            // Try connecting for 10 seconds. Otherwise just continue.

#define MY_RF24_CHANNEL 107                         // In EU the default channel 76 overlaps with wifi, so you could try using channel 100. But you will have to set this up on every device, and also on the controller.
// #define MY_RF24_DATARATE RF24_250KBPS            // Slower datarate makes the network more stable?
// #define MY_RF24_DATARATE RF24_1MBPS              // Slower datarate makes the network more stable?
// #define MY_RF24_DATARATE RF24_2MBPS              // Slower datarate makes the network more stable?

// #define MY_NODE_ID (AUTO)
// #define MY_NODE_ID 1                             // Giving a node a manual ID can in rare cases fix connection issues.
// #define MY_PARENT_NODE_ID 0                      // Fixating the ID of the gatewaynode can in rare cases fix connection issues.
// #define MY_PARENT_NODE_IS_STATIC                 // Used together with setting the parent node ID. Daking the controller ID static can in rare cases fix connection issues.

#define MY_SPLASH_SCREEN_DISABLED                   // Saves a little memory.
#define MY_DISABLE_RAM_ROUTING_TABLE_FEATURE        // Saves a little memory.